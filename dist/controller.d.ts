import 'reflect-metadata';
import { Type } from './type';
export interface RESTController extends Type<any> {
}
export declare function Controller(path: string, middleware?: Function[]): <T extends new (...args: any[]) => {}>(constructor: T) => {
    new (...args: any[]): {
        path: string;
        middleware: Function[];
    };
} & T;
export declare function GetRequest<T>(url: string, middleware?: Function[], disableResponse?: boolean): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function PostRequest<T>(url: string, middleware?: Function[], disableResponse?: boolean): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function PutRequest<T>(url: string, middleware?: Function[], disableResponse?: boolean): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
export declare function DeleteRequest<T>(url: string, middleware?: Function[], disableResponse?: boolean): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
