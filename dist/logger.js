"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var winston_1 = require("winston");
var LogLevel;
(function (LogLevel) {
    LogLevel["Silly"] = "silly";
    LogLevel["Verbose"] = "verbose";
    LogLevel["Debug"] = "debug";
    LogLevel["Info"] = "info";
    LogLevel["Warn"] = "warn";
    LogLevel["Error"] = "error";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
function Logger(label) {
    return function (target, key) {
        LoggerResolver.register(label, target, key);
    };
}
exports.Logger = Logger;
var LoggerResolver = /** @class */ (function () {
    function LoggerResolver() {
    }
    LoggerResolver.register = function (label, target, key) {
        LoggerResolver.instances.push({ label: label, target: target, key: key });
    };
    LoggerResolver.resolve = function (config) {
        if (config && config.disabled)
            return;
        var loggerFormat = winston_1.format.printf(function (info) {
            return info.timestamp + " [" + info.label + "] " + info.level + ": " + info.message;
        });
        var consoleLevel;
        var fileLevel;
        var fileName;
        if (!config) {
            consoleLevel = LogLevel.Silly;
        }
        else {
            var consoleOptions = config.console;
            var fileOptions = config.file;
            if (!consoleOptions) {
                consoleLevel = LogLevel.Silly;
            }
            else if (!consoleOptions.disabled) {
                consoleLevel = consoleOptions.level || LogLevel.Silly;
            }
            if (fileOptions && !fileOptions.disabled) {
                fileLevel = fileOptions.level;
                fileName = fileOptions.name;
            }
        }
        LoggerResolver.instances.forEach(function (el) {
            var loggerTransports = [];
            if (consoleLevel) {
                loggerTransports.push(new winston_1.transports.Console({
                    format: winston_1.format.combine(winston_1.format.colorize(), loggerFormat),
                    level: consoleLevel,
                }));
            }
            if (fileLevel && fileName) {
                loggerTransports.push(new winston_1.transports.File({
                    filename: fileName,
                    format: loggerFormat,
                    level: fileLevel,
                }));
            }
            el.target[el.key] = winston_1.loggers.get(el.label, {
                format: winston_1.format.combine(winston_1.format.label({ label: el.label }), winston_1.format.timestamp()),
                transports: loggerTransports,
            });
        });
    };
    LoggerResolver.instances = [];
    return LoggerResolver;
}());
exports.LoggerResolver = LoggerResolver;
//# sourceMappingURL=logger.js.map