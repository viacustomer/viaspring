import { Request } from 'express';
import { Controller, DeleteRequest, GetRequest, Logger, PostRequest, PutRequest, Transaction } from '../dist';
import { DemosRepository } from './test.repository';
import { TransactionService } from './transaction.service';

class BaseController {
    logger;

    constructor(label: string) {

    }
}

@Controller('/test')
export class TestController {
    @Logger('DEMOS')
    logger;

    constructor(private testRepo: DemosRepository, private transaction: TransactionService) {
    }

    @GetRequest('')
    async test(req: Request): Promise<any> {
        return this.transaction.run(this, async (session) => {
            this.logger.info('SESSION', session);
            // return Promise.reject({msg: 'SUPER ERROR', status: 401});
            return await this.testRepo.getAllDemos();
        });
    }

    @PostRequest('')
    async test2(req: Request): Promise<any> {
        // return Promise.reject({msg: 'SUPER ERROR', status: 401});
        return await this.testRepo.getAllDemos();
    }

    @PutRequest('')
    async test3(req: Request): Promise<any> {
        // return Promise.reject({msg: 'SUPER ERROR', status: 401});
        return await this.testRepo.getAllDemos();
    }


    @DeleteRequest('')
    async test4(req: Request): Promise<any> {
        // return Promise.reject({msg: 'SUPER ERROR', status: 401});
        return await this.testRepo.getAllDemos();
    }
}
