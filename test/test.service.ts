import { Injectable, Logger, Inject, SERVER_PROVIDER } from '../dist';
import { Server } from 'http';
import { Test2Service } from './test2.service';
import { TEST_TOKEN1, TEST_TOKEN2 } from './models';

@Injectable()
export class TestService {
    @Logger('TEST LOGGER')
    logger;

    constructor(@Inject(SERVER_PROVIDER) private server: Server, private ts: Test2Service,
                @Inject(TEST_TOKEN2) private tt: { test: number }) {
    }

    init(): void {

    }
}