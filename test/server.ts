import { bootstrap, InjectionToken, Logger, LogLevel, MongoService, Server } from '../dist';
import { TestService } from './test.service';
import { AppInitializer } from './initializer';
import { TestController } from './test.controller';
import { Test2Service } from './test2.service';
import { TEST_TOKEN1, TEST_TOKEN2, TEST_TOKEN3 } from './models';
import { DemosRepository } from './test.repository';
import { TransactionService } from './transaction.service';

@Server({
    port: 3000,
    initializer: AppInitializer,
    cors: false,
    providers: [
        {
            provide: TEST_TOKEN2,
            value: {test: 456},
        },
        {
            provide: TEST_TOKEN1,
            value: {test: 123},
        },
        Test2Service,
        TestService,
        TransactionService,
        MongoService,
        {
            provide: TEST_TOKEN3,
            value: {test: 789},
        },
        DemosRepository,
    ],
    controllers: [
        TestController,
    ],
    loggerConfig: {
        console: {
            level: LogLevel.Info,
        },
        file: {
            name: 'error.log',
            level: LogLevel.Error,
        },
    },
})
class AppServer {
}

bootstrap<AppServer>(AppServer);
