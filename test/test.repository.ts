import { MongoService, Injectable, Logger } from '../dist';

@Injectable()
export class DemosRepository {

    constructor(
        private mongo: MongoService,
    ) {
    }

    getAllDemos(): Promise<any[]> {
        return this.mongo.collection('demos').find().toArray();
    }
}
