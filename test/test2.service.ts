import { Injectable, Logger, Inject } from '../dist';
import { TEST_TOKEN1 } from './models';

@Injectable()
export class Test2Service {

    constructor(@Inject(TEST_TOKEN1) private tt: { test: number }) {
    }

    hello(): void {
        console.log(this.tt);
    }
}