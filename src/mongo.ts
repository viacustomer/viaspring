import { MongoClient, Collection, DbCollectionOptions } from 'mongodb';
import { Injectable, Injector } from './di';
import { Logger } from './logger';

export interface IMongoConfig {
    url: string;
}

@Injectable()
export class MongoService {
    @Logger('MONGO')
    private logger;
    private _client: MongoClient;

    get client(): MongoClient {
        return this._client;
    }

    connect(config: IMongoConfig): Promise<MongoClient> {
        this.logger.info(`CONNECTING TO ${config.url}`);
        return new Promise((resolve, reject) => {
            MongoClient.connect(config.url, {useNewUrlParser: true}, (err, c) => {
                if (err) {
                    this.logger.error(`CONNECTION ERROR | ${err}`);
                    reject(err);
                } else {
                    this._client = c;
                    this.logger.info('CONNECTED TO DATABASE');
                    resolve(this._client);
                }
            });
        });
    }

    collection(colName: string): Collection {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        return this._client.db().collection(colName);
    }

    createCollection(colName: string, options?: DbCollectionOptions): Promise<Collection<any>> {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        return this._client.db().createCollection(colName, options || {});
    }

    removeCollection(colName: string): Promise<void> {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        return this._client.db().collection(colName).drop();
    }

    terminate(): void {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        this._client.close();
    }
}

export function Transaction() {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        const initialValue = descriptor.value;
        descriptor.value = async function () {
            let client;
            const mongoService = <MongoService>Injector.resolve(MongoService);
            if (mongoService) {
                client = mongoService.client;
            }
            target[propertyKey].client = client;
            try {
                const r = await initialValue.call(this);
                console.log('TA SUCCESS');
                return r;
            } catch (e) {
                console.log('TA ERROR');
                return Promise.reject(e);
            }
        };
        return descriptor;
    };
}
