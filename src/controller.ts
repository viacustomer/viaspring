import 'reflect-metadata';
import * as express from 'express';
import { Request } from 'express';
import { Response } from 'express';
import { Type } from './type';
import { Sentry } from './server';

export interface RESTController extends Type<any> {
}

enum HTTPMethod {
    GET = 'get',
    POST = 'post',
    PUT = 'put',
    DELETE = 'delete',
}

export function Controller(path: string, middleware: Function[] = []) {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        return class extends constructor {
            path = path;
            middleware = middleware;

            constructor(...args: any[]) {
                super(...args);
            }
        };
    };
}

function handleError(err: any, res: Response): string {
    const msg = err.message || err.msg || 'SERVER ERROR';
    const status = err.status || 500;
    res.status(status).send({err: msg});
    return msg;
}

function request<T>(method: HTTPMethod, url: string, middleware: Function[] = [], disableResponse?: boolean) {
    if (url.substring(0, 1) !== '/') url = `/${url}`;
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        const initialValue = descriptor.value;
        if (!target['__requests']) {
            target['__requests'] = [];
        }
        target['__requests'].push(propertyKey);
        descriptor.value = function () {
            const router = express.Router();
            const groupMiddleware = arguments[0];
            return router[method](url, [...groupMiddleware, ...middleware], async (req: Request, res: Response) => {
                const logger = target.logger;
                const startTimeStamp = new Date();
                if (logger) {
                    logger.info(`[${method.toUpperCase()} | REQ] | ${getFullUrlFromRequest(req)}` +
                        ` | PARAMS:${getParamsFromRequest(req)} | QUERY:${getQueryFromRequest(req)}` +
                        ` | IP:${getIpAddressFromRequest(req)}` +
                        ` | BODY: ${getBodyFromRequest(req)}`);
                }
                try {
                    const r: T = await initialValue.call(this, req, res);
                    if (res.headersSent || disableResponse) {
                        return;
                    }
                    const successTimeStamp = new Date();
                    const successDuration = Math.abs(<any>successTimeStamp - <any>startTimeStamp);
                    if (logger) {
                        logger.info(`[${method.toUpperCase()} | RES] | ${getFullUrlFromRequest(req)}` +
                        ` | Response time: ${successDuration} ms` +
                        ` | IP: ${getIpAddressFromRequest(req)} | Status: ${res.statusCode}` +
                        ` | RESPONSE: ${JSON.stringify(r)}`);
                    }
                    res.send(r);
                } catch (e) {
                    if (res.headersSent) {
                        return;
                    }
                    const msg = handleError(e, res);
                    const failTimeStamp = new Date();
                    const failDuration = Math.abs(<any>failTimeStamp - <any>startTimeStamp);
                    try {
                        (() => {
                            Sentry.captureException(`[${method.toUpperCase()}] | ${getFullUrlFromRequest(req)} | 
                            Status: ${e.statusCode || 500} | Response time: ${failDuration} ms | IP: ${getIpAddressFromRequest(req)} | ERROR: ${msg}`);
                        })
                    } catch (e) {
                    }
                    if (logger) {
                        logger.error(`[${method.toUpperCase()}] | ${getFullUrlFromRequest(req)} | 
                        Status: ${e.statusCode  || 500} | Response time: ${failDuration} ms | IP: ${getIpAddressFromRequest(req)} | ERROR: ${msg}`);
                    }
                }
            });
        };
        return descriptor;
    };
}

export function GetRequest<T>(url: string, middleware: Function[] = [], disableResponse?: boolean) {
    return request(HTTPMethod.GET, url, middleware, disableResponse);
}

export function PostRequest<T>(url: string, middleware: Function[] = [], disableResponse?: boolean) {
    return request(HTTPMethod.POST, url, middleware, disableResponse);
}

export function PutRequest<T>(url: string, middleware: Function[] = [], disableResponse?: boolean) {
    return request(HTTPMethod.PUT, url, middleware, disableResponse);
}

export function DeleteRequest<T>(url: string, middleware: Function[] = [], disableResponse?: boolean) {
    return request(HTTPMethod.DELETE, url, middleware, disableResponse);
}

function getFullUrlFromRequest(req: Request): string {
    return req.originalUrl;
}

function getParamsFromRequest(req: Request): string {
    return JSON.stringify(req.params);
}

function getQueryFromRequest(req: Request): string {
    return JSON.stringify(req.query);
}

function getIpAddressFromRequest(req: Request): string {
    const ip = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress;
    return JSON.stringify(ip);
}

function getBodyFromRequest(req: Request) {
    try {
        return JSON.stringify(req.body);
    } catch (e) {
        return typeof req.body;
    }
}
