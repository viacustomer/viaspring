import { format, loggers, transports } from 'winston';

export enum LogLevel {
    Silly = 'silly',
    Verbose = 'verbose',
    Debug = 'debug',
    Info = 'info',
    Warn = 'warn',
    Error = 'error',
}

export interface ILoggerConfig {
    disabled?: boolean;
    console?: {
        disabled?: boolean;
        level: LogLevel | string;
    },
    file?: {
        disabled?: boolean;
        name: string;
        level: LogLevel | string;
    }
}

export function Logger(label: string) {
    return (target, key) => {
        LoggerResolver.register(label, target, key);
    }
}

export class LoggerResolver {
    static instances: { label: string, target: any, key: string }[] = [];

    static register(label: string, target: any, key: string): void {
        LoggerResolver.instances.push({label, target, key});
    }

    static resolve(config: ILoggerConfig) {
        if (config && config.disabled) return;

        const loggerFormat = format.printf(info => {
            return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
        });
        let consoleLevel;
        let fileLevel;
        let fileName;

        if (!config) {
            consoleLevel = LogLevel.Silly;
        } else {
            const consoleOptions = config.console;
            const fileOptions = config.file;

            if (!consoleOptions) {
                consoleLevel = LogLevel.Silly;
            } else if (!consoleOptions.disabled) {
                consoleLevel = consoleOptions.level || LogLevel.Silly;
            }

            if (fileOptions && !fileOptions.disabled) {
                fileLevel = fileOptions.level;
                fileName = fileOptions.name;
            }
        }

        LoggerResolver.instances.forEach((el) => {
            const loggerTransports = [];
            if (consoleLevel) {
                loggerTransports.push(
                    new transports.Console({
                        format: format.combine(
                            format.colorize(),
                            loggerFormat,
                        ),
                        level: consoleLevel,
                    }),
                )
            }
            if (fileLevel && fileName) {
                loggerTransports.push(
                    new transports.File({
                        filename: fileName,
                        format: loggerFormat,
                        level: fileLevel,
                    }),
                )
            }

            el.target[el.key] = loggers.get(el.label, {
                format: format.combine(
                    format.label({label: el.label}),
                    format.timestamp(),
                ),
                transports: loggerTransports,
            });
        });
    }
}